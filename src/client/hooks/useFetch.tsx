import { useCallback, useState } from 'react';

interface IUseFetch {
    url: string;
    method?: 'get' | 'post' | 'delete' | 'put';
}

const parseToJson = async (response: Response) => {
    const data = await response.json();
    return data;
};

const useFetch = ({
    url,
    method = 'get',
}: IUseFetch): [(request?: Object) => void, any | null, string | null, boolean] => {
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    const requestFunc: (request?: Object) => void = useCallback(
        async (request?: { body?: BodyInit; params?: { [key: string]: any } }) => {
            try {
                setError(null);
                setIsLoading(true);
                let requestUrl = `${`http://${window.location.hostname}:8080/api`}${url}`;
                const options: RequestInit = {
                    headers: { 'Content-Type': 'application/json' },
                    method,
                };
                if (request?.body) {
                    options.body = request.body;
                }

                if (request?.params) {
                    Object.keys(request.params).forEach((key) => {
                        if (typeof request?.params?.[key] === 'object') {
                            request.params[key] = JSON.stringify(request?.params?.[key]);
                        }
                    });
                    requestUrl += `?${new window.URLSearchParams(request.params).toString()}`;
                }
                const response = await window.fetch(requestUrl, options);
                const result = await parseToJson(response);
                if (response.status < 200 || response.status > 399) {
                    throw new Error(result);
                }
                setData(result);
                setIsLoading(false);
            } catch (err) {
                // setError(err.message);
                setIsLoading(false);
            }
        },
        [url, method]
    );

    return [requestFunc, data, error, isLoading];
};

export default useFetch;
