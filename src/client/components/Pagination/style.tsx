import styled from '@emotion/styled';

export const Button = styled.button`
    width: 10rem;
    height: 3rem;
`;

export const PaginatorContainer = styled.div`
    display: flex;
    flex-direction: row;
`;
