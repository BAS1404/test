import React, { Fragment } from 'react';
import { PaginatorContainer, Button } from './style';

const BetweenButton = () => <Button disabled>...</Button>;

const Paginator = ({
    currentPage,
    onPageChange,
    pageCount,
    width,
}: {
    currentPage: number;
    onPageChange: (page: number) => void;
    range: number | undefined;
    pageCount: number;
    width: number;
}) => {
console.log(currentPage);
    return (
        <PaginatorContainer style={{ width: `${width}px` }}>
            {pageCount > 0 ? <Button onClick={() => onPageChange(1)}>{1}</Button> : null}
            {pageCount > 1 ? (
                <Fragment>
                    {currentPage>1 ? <Button onClick={()=>onPageChange(currentPage-1)}>Предыдущая страница</Button>:null}
                                        {currentPage<pageCount ? <Button onClick={()=>onPageChange(currentPage+1)}>Следующая страница</Button>:null}

                    <Button onClick={() => onPageChange(pageCount)}>{pageCount}</Button>
                </Fragment>
            ) : null}
        </PaginatorContainer>
    );
};

export default Paginator;
