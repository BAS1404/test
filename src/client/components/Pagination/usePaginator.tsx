import React, { useState, useCallback, useMemo } from 'react';
import { SortDirectionType, SortDirection } from 'react-virtualized';

const usePaginator = ({
    perPage = 1,
    limit,
    list = [],
}: {
    perPage?: number;
    limit: number;
    list: Array<Object>;
}): [
    number,
    Array<Object>,
    (arg0: number) => void,
    (arg0: { sortBy: string; sortDirection: SortDirectionType }) => void
] => {
    const [page, setPage] = useState(perPage || 1);
    const handleChangePage = (currentPage: number): void => {
        console.log(currentPage);
        setPage(Number(currentPage));
    };
    console.log(page);
    const [currentSortBy, setCurrentSortBy] = useState('id');
    const [currentSortDirection, setCurrentSortDirection] = useState('');

    const sortedList = useMemo(() => {
        const asc = currentSortDirection === SortDirection.ASC;
        const compare = (a: any, b: any) => {
            if (a[currentSortBy] < b[currentSortBy]) {
                return asc ? -1 : 1;
            }
            if (a[currentSortBy] > b[currentSortBy]) {
                return asc ? 1 : -1;
            }
            return 0;
        };

        return list !== null ? list.sort(compare) : [];
    }, [currentSortBy, currentSortDirection, list]);

    const sort = ({ sortBy, sortDirection }: { sortBy: string; sortDirection: SortDirectionType }) => {
        setCurrentSortBy(sortBy);
        setCurrentSortDirection(sortDirection);
    };

    return [page, sortedList, handleChangePage, sort];
};

export default usePaginator;
