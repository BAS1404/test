import React, { ReactNode, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { ModalWindow, ModalContainer, ModalHeader, ModalBody } from './styles';

const modalRoot = document.getElementById('modal')!;

const Modal = ({ children, header }: { children: ReactNode; header?: string | ReactNode }) => {
    useEffect(() => {
        const el = document.createElement('div');
        modalRoot.appendChild(el);
        return () => {
            modalRoot.removeChild(el);
        };
    }, []);
    return ReactDOM.createPortal(
        <ModalWindow>
            <ModalContainer>
                {header && <ModalHeader>{header}</ModalHeader>}
                <ModalBody>{children}</ModalBody>
            </ModalContainer>
        </ModalWindow>,
        modalRoot
    );
};

export default Modal;
