import React, { useState } from "react";
const useModal = (initialStatus?: boolean) => {
  const [visible, setVisible] = useState(
    initialStatus !== undefined ? initialStatus : false
  );
  const handleVisibleModal = (status?: boolean) => {
    setVisible(status !== undefined ? status : !visible);
  };
  return [visible, handleVisibleModal];
};

export default useModal;
