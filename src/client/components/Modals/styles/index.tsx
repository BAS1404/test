import styled from "@emotion/styled";

export const ModalWindow = styled.div({
  position: "fixed",
  top: "0",
  left: "0",
  width: "100vw",
  height: "100vh",
  zIndex: "999",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  backgroundColor: "rgba(0, 0, 0, 0.45)",
});

export const ModalContainer = styled.div({
  width: "50%",
  padding: "0.625rem",
  height: "auto",
  backgroundColor: "#ffffff",
  top: "50%",
});

export const ModalHeader = styled.div({
  width: "100%",
  marginTop: "1.25rem",
  marginBottom: "1.25rem",
  textTransform: "uppercase",
  textAlign: "center",
  fontSize: "1.25rem",
});

export const ModalBody = styled.div({});
