import React from 'react';
import 'react-virtualized/styles.css';
import { Column, Table, SortDirection, AutoSizer, SortDirectionType } from 'react-virtualized';
import { Paginator } from '../Pagination';

interface ITableProps {
    readonly sortedList: Array<Object>;
    readonly sort: () => void;
    readonly sortBy: string;
    readonly sortDirection: SortDirectionType;
    readonly columns: Array<{ label: string; dataKey: string }>;
    currentPage: number;
    onPageChange: (arg0: number) => void;
    readonly range: number | undefined;
    readonly pageCount: number;
}

const Tables = ({
    sortedList,
    sort,
    sortBy,
    sortDirection,
    columns,
    currentPage,
    onPageChange,
    range,
    pageCount,
}: ITableProps) => {
    return (
        <AutoSizer>
            {({ height, width }: { height: number; width: number }) => (
                <>
                    <Table
                        width={width}
                        height={350}
                        headerHeight={20}
                        rowHeight={30}
                        sort={sort}
                        sortBy={sortBy}
                        sortDirection={sortDirection}
                        rowCount={sortedList.length}
                        rowGetter={({ index }: { index: number }) => sortedList[index]}
                    >
                        {columns.map((item, index) => (
                            <Column
                                label={item?.label}
                                dataKey={item?.dataKey}
                                width={200}
                                key={`table${currentPage}-index`}
                            />
                        ))}
                    </Table>
                    <Paginator
                        width={width}
                        currentPage={currentPage}
                        onPageChange={onPageChange}
                        range={range}
                        pageCount={pageCount}
                    />
                </>
            )}
        </AutoSizer>
    );
};

export default Tables;
