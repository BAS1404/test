import styled from '@emotion/styled';
import { keyframes, css } from '@emotion/react';

const CircleAnimation = keyframes`
	0% { "transform": "rotate(0deg)" }
	100% { transform: rotate(360deg); }
`;

export const LoaderWrapper = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    background-color: rgba(0, 0, 0, 0.65);
    width: 100vw;
    height: 100vh;
    z-index:9999;
`;

export const CircleLoader = styled.div`
    height: 4rem;
    width: 4rem;
    background-color: lightblue;
    margin-left: 47vw;
    margin-top: 46vh;
    ${css`
        animation: ${CircleAnimation} 1s infinite;
    `}
`;
