import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import { useDispatch, useSelector } from 'react-redux';
import { setLoaderStatusThinkFunction } from '../../../store/actions/loader';
import { Modal } from '../../Modals';
import { CircleLoader, LoaderWrapper } from './style';

const LoaderRoot = document.getElementById('loader')!;

const CircleLoaderComponent = ({ loading }: { loading: Boolean }) => {
    const dispatch = useDispatch();

    useEffect(() => {
        const el = document.createElement('div');
        LoaderRoot.appendChild(el);
        return () => {
            LoaderRoot.removeChild(el);
        };
    }, []);
    return loading
        ? ReactDOM.createPortal(
              <LoaderWrapper>
                  <CircleLoader />
              </LoaderWrapper>,
              LoaderRoot
          )
        : null;
};

export default CircleLoaderComponent;
