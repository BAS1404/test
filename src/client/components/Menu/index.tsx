import React from "react";
import { MenuContainer, MenuItem, MenuWrapper } from "./style";

const Menu = () => {
  return (
    <MenuContainer>
      <MenuWrapper>
        <MenuItem to="/">Журнал состояния датчиков</MenuItem>
        <MenuItem to="/users">Пользователи</MenuItem>
        <MenuItem to="/sensors">Список устройств</MenuItem>
        <MenuItem to="/states">Состояние устройств</MenuItem>
        <MenuItem to="/scripts">Скрипты</MenuItem>
      </MenuWrapper>
    </MenuContainer>
  );
};

export default Menu;
