import styled from "@emotion/styled";
import { Link } from "react-router-dom";

export const MenuContainer = styled.menu`
  width: 100%;
  height: 10vh;
  background-color: lightblue;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 0;
`;

export const MenuWrapper = styled.ul`
  display: flex;
  justify-content: space-evenly;
  flex-direction: row;
  padding: 0;
`;

export const MenuItem = styled(Link)`
  &:link {
    color: white;
    text-decoration: none;
    font-size: 1.2rem;
  }

  &:visited {
    color: white;
    text-decoration: none;
    font-size: 1.2rem;
  }

  &:hover {
    color: white;
    text-decoration: none;
    font-size: 1.2rem;
  }

  &:active {
    color: white;
    text-decoration: none;
    font-size: 1.2rem;
  }
`;
