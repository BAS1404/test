import styled from "@emotion/styled";
import { Routes } from "react-router-dom";
export default styled(Routes)`
  width: 100%;
  height: 90vh;
`;
