import React, { Fragment, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { SortDirection } from 'react-virtualized';
import { Paginator, Table, usePaginator } from '../../components';
import { useFetch } from '../../hooks';
import { setLoaderStatusThinkFunction } from '../../store/actions/loader';

const SensorPage = () => {
    const limit = 20;
    const dispatch = useDispatch();
    const [requestFunc, data, error, isLoading] = useFetch({ url: '/sensor' });

    useEffect(() => {
        requestFunc({ params: { page:1, limit } });
    }, []);

    console.log(data);
    const handleChangePage = (page: number) => {
        requestFunc({ params: { page, limit } });
    };
    const [page, sortedList, changePage, sort] = usePaginator({
        perPage: 1,
        limit: limit,
        list: data?.list,
    });

    useEffect(() => {
        dispatch(setLoaderStatusThinkFunction(isLoading));
        return () => {
            dispatch(setLoaderStatusThinkFunction(false));
        };
    }, [isLoading]);
    return (
        <Fragment>
            <Table
                sortedList={sortedList}
                sortBy={'name'}
                sortDirection={SortDirection.ASC}
                sort={() => {}}
                currentPage={page}
                columns={[
                    { label: 'Устройство', dataKey: 'name' },
                    { label: 'Описание', dataKey: 'description' },
                ]}
                onPageChange={handleChangePage}
                range={3}
                pageCount={data?.count}
            />
        </Fragment>
    );
};

export default SensorPage;
