import { LOADER_SWITCH } from '../types';

interface ILoaderReducer {
    loading: boolean;
}

const initialState: ILoaderReducer = { loading: false};

const loaderReducer = (state = initialState, action: { type: string; payload: any }) => {
    switch (action.type) {
        case LOADER_SWITCH:
            return { ...state, loading: action.payload };
        default:
            return state;
    }
};

export default loaderReducer;