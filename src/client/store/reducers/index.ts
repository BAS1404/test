import { combineReducers } from 'redux';
import LoaderReducer from './loader';

export default combineReducers({ LoaderReducer });
