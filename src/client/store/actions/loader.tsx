import { Dispatch } from 'redux';
import { LOADER_SWITCH } from '../types';

const setLoaderStatus = (payload: boolean) => ({
    type: LOADER_SWITCH,
    payload,
});

export const setLoaderStatusThinkFunction = (payload: boolean) => {
    return (dispatch: Dispatch, getState: () => { LoaderReducer: { loading: boolean } }) => {
        const { loading } = getState().LoaderReducer;
        if (payload!== loading) {
            dispatch(setLoaderStatus(payload));
        }
    };
};
