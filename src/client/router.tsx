import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, useParams } from 'react-router-dom';
import { Menu, ContentRoutes, CircleLoader } from './components';
import Users from './pages/users';
import Scripts from './pages/scripts';
import Sensor from './pages/sensor';
import SensorsJournal from './pages/sensors';
import States from './pages/states';
import { useSelector } from 'react-redux';

export default () => {
    const loading = useSelector((state: { LoaderReducer: { loading: Boolean } }) => state?.LoaderReducer?.loading);
    return (
        <Fragment>
            <Router>
                <Menu />
                <CircleLoader loading={loading || false} />
                <ContentRoutes>
                    <Route path="/" element={<SensorsJournal />} />
                    <Route path="/sensors" element={<Sensor />} />
                    <Route path="/states" element={<States />} />
                    <Route path="/scripts" element={<Scripts />} />
                    <Route path="/users" element={<Users />} />
                </ContentRoutes>
            </Router>
        </Fragment>
    );
};
