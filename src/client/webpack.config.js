const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const { HotModuleReplacementPlugin } = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { version } = require('./package.json');
const { NODE_ENV } = process.env;
const ESLintPlugin = require('eslint-webpack-plugin');
const isDev = NODE_ENV === 'development';
console.log(version);
console.log(`Development mode: ${isDev}`);

module.exports = {
    mode: isDev ? 'development' : 'production',
    entry: './index.tsx',
    output: {
        path: path.resolve(__dirname, '../../dist/public'),
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.ts(x)$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
                type: 'asset/resource',
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.json', '.wasm'],
    },
    devServer: {
        compress: true,
        port: 9000,
    },
    plugins: [
        new ESLintPlugin(),
        new htmlWebpackPlugin({
            template: 'index.html',
        }),
        new CleanWebpackPlugin(),
        new HotModuleReplacementPlugin(),
    ],
};
