import { Router } from "express";
import { RestController } from "../controllers";
import { StateModel } from "../models";
const UsersController = new RestController(StateModel);
const userRouter = Router()
  .get("/", UsersController.listPagination)
  .get("/:id", UsersController.recordById)
  .post("/", UsersController.create)
  .put("/:id", UsersController.update)
  .delete("/", UsersController.delete);
export default userRouter;
