import { Router } from "express";
import { RestController } from "../controllers";
import { SensorsModel } from "../models";

const SensorsController = new RestController(SensorsModel);

const scriptsRouter = Router()
  .get("/", SensorsController.listPagination)
  .get("/:id", SensorsController.recordById)
  .post("/", SensorsController.create)
  .put("/:id", SensorsController.update)
  .delete("/", SensorsController.delete);

export default scriptsRouter;
