import { Router } from "express";
import scriptRouter from "./scripts.router";
import sensorsRouter from "./sensors.router";
import sensorRouter from "./sensor.router";
import stateRouter from "./state.router";
import usersRouter from "./users.router";

export default Router()
  .use("/scripts", scriptRouter)
  .use("/sensors", sensorsRouter)
  .use("/sensor", sensorRouter)
  .use("/users", usersRouter)
  .use("/state", stateRouter);
