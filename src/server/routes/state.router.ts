import { Router } from "express";
import { RestController } from "../controllers";
import { StateModel } from "../models";
const StateController = new RestController(StateModel);
const stateRouter = Router()
  .get("/", StateController.listPagination)
  .get("/:id", StateController.recordById)
  .post("/", StateController.create)
  .put("/:id", StateController.update)
  .delete("/", StateController.delete);
export default stateRouter;
