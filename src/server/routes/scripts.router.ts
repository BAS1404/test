import { Router } from "express";
import { RestController } from "../controllers";
import { ScriptModel } from "../models";
const ScriptsController = new RestController(ScriptModel);
const scriptsRouter = Router()
  .get("/", ScriptsController.listPagination)
  .get("/:id", ScriptsController.recordById)
  .post("/", ScriptsController.create)
  .put("/:id", ScriptsController.update)
  .delete("/", ScriptsController.delete);
export default scriptsRouter;
