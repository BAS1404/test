import { Router } from "express";
import { RestController } from "../controllers";
import { SensorModel } from "../models";
const SensorController = new RestController(SensorModel);
const sensorRouter = Router()
  .get("/", SensorController.listPagination)
  .get("/:id", SensorController.recordById)
  .post("/", SensorController.create)
  .put("/:id", SensorController.update)
  .delete("/", SensorController.delete);
export default sensorRouter;
