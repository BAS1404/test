import * as Mongoose from "mongoose";
import { Request, Response } from "express";
import { sensorsList, sensorsListPaginated } from "./customFinder";

class RestController {
  private readonly modelDB: Mongoose.Model<any>;

  constructor(model: Mongoose.Model<any>) {
    if (model) {
      this.modelDB = model;
    } else throw new Error("No modal for request");
  }

  listAll = async (req: Request, res: Response) => {
    let result:any;
    switch (this.modelDB.modelName.toLocaleLowerCase()) {
      case "sensors":
        {
          result = await sensorsList();
        }
        break;
      default:
        result = await this.modelDB.find();
    }
    res.status(200).json(result);
  };

  listPagination = async (req: Request, res: Response) => {
    const page = (parseInt(<string>req.query.page) || 1) - 1;
    const limit = parseInt(<string>req.query.limit) || 10;
    let result:any;
    const count = Math.floor(
      (await this.modelDB.find().count()) / limit
    );
    switch (this.modelDB.modelName.toLocaleLowerCase()) {
      case "sensors":
        {
          result = await sensorsListPaginated(page, limit);
        }
        break;
      default:
        result = await this.modelDB
          .find()
          .skip(page * limit)
          .limit(limit);
    }
    res.status(200).json({ list: result, count, page: req.query.page });
  };
  recordById = async (req: Request, res: Response) => {
    const { id } = req.params;
    if (id) {
      try {
        const result = await this.modelDB.findById(id);
        if (result) {
          res.status(200).json(result);
        }
      } catch (e) {
        res.status(500).json(e);
      }
    } else res.status(404);
  };

  create = async (req: Request, res: Response) => {
    try {
      const data = req.body;
      const newRecord = new this.modelDB({ data });
      const result = newRecord.save();
      res.status(200).json(result);
    } catch (e) {
      res.send(500).json(e);
    }
  };

  update = async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      const data = req.body;
      const newRecord = await this.modelDB.findOneAndUpdate({ _id: id }, data, {
        returnOriginal: false,
      });
      res.status(200).json(newRecord);
    } catch (e) {
      res.status(500).json(e);
    }
  };
  delete = async (req: Request, res: Response) => {
    try {
      const { id } = req.body;
      await this.modelDB.deleteOne({ _id: id });
      res.status(200).json(id);
    } catch (e) {
      res.status(500).json(e);
    }
  };
}

export default RestController;
