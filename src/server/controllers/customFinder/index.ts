import { SensorsModel } from "../../models";
import { ObjectId } from "mongoose";
interface ISensorsList {
  _id: ObjectId;
  user: {
    name: string;
  };
  name: {
    name: string;
  };
  state: {
    name: string;
  };
  skudEmployee: {
    name: string;
  };
  script: {
    name: string;
  };
  timeEventSensorAt: {
    name: string;
  };
  updated_at: {
    name: string;
  };
  created_at: {
    name: string;
  };
}
export const sensorsList = () =>
  new Promise(async (resolve, reject) => {
    try {
      const findData: Array<ISensorsList> = await SensorsModel.find()
        .populate("user")
        .populate("name")
        .populate("state")
        .populate("skudEmployee")
        .populate("script");
      const result = findData.map((item) => ({
        userName: item.user.name,
        device: item.name.name,
        script: item.script.name,
        skudEmployee: item?.skudEmployee?.name,
        state: item.state.name,
        timeEventSensorAt: item.timeEventSensorAt,
        created_at: item.created_at,
        updated_at: item.updated_at,
      }));
      resolve(result);
    } catch (e) {
      reject(e);
    }
  });

export const sensorsListPaginated = (page: number, limit: number) =>
  new Promise(async (resolve, reject) => {
    try {
      const findData: Array<ISensorsList> = await SensorsModel.find()
        .skip(page * limit)
        .limit(limit)
        .populate("user")
        .populate("name")
        .populate("state")
        .populate("skudEmployee")
        .populate("script");
      const result = findData.map((item) => ({
        userName: item.user.name,
        device: item.name.name,
        script: item.script.name,
        skudEmployee: item?.skudEmployee?.name,
        state: item.state.name,
        timeEventSensorAt: item.timeEventSensorAt,
        created_at: item.created_at,
        updated_at: item.updated_at,
      }));

      resolve(result);
    } catch (e) {
      reject(e);
    }
  });
