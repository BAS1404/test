import "dotenv/config";
import { SensorsModel } from "./models";
import { version } from "./package.json";
import { connect, connection } from "mongoose";
import { createExpress } from "./app";

const {
  MONGO_ROOT_USERNAME,
  MONGO_ROOT_PASSWORD,
  MONGO_ROOT_ADDRESS,
  MONGO_ROOT_PORT,
  MONGO_ROOT_DATABASE,
} = process.env;

const createApp = async () => {
  try {
    connect(
      `mongodb://${MONGO_ROOT_USERNAME}:${MONGO_ROOT_PASSWORD}@${MONGO_ROOT_ADDRESS}:${MONGO_ROOT_PORT}`,
      {
        dbName: MONGO_ROOT_DATABASE,
      }
    );
    connection.on("connected", () => {
      console.log(`Version: ${version}`);
      createExpress();
    });
  } catch (e) {
    return "Ошибка подключения к базе данных";
  }
};
createApp();
