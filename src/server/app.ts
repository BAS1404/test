import express, { Express } from "express";
import routes from "./routes";
import bodyParser from "body-parser";
import cors from 'cors';

export const createExpress = () => {
  const app: Express = express();
  const api: Express = express();
  app.use(cors());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(express.static('public'));
  const port = process.env.PORT || 8080;

  api.use("/", routes);

  app.use("/api", api);
  app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
  });
};
