import { Document, model, ObjectId, Schema } from "mongoose";

interface IUsers extends Document {
  _id: ObjectId;
  fio: string;
  phone?: string;
  type: ObjectId;
}

const usersSchema = new Schema<IUsers>(
  {
    fio: { type: String, required: true },
    phone: { type: String, required: false },
    type: { type: Schema.Types.ObjectId, ref: "usertypes" },
  },
  {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

const usersModel = model<IUsers>("users", usersSchema);

export default usersModel;
