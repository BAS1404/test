import { Document, model, ObjectId, Schema } from "mongoose";

interface IScripts extends Document {
  _id: ObjectId;
  name: string;
  description: string;
}

const scriptsSchema = new Schema<IScripts>(
  {
    name: { type: String, required: true },
    description: { type: String, required: false },
  },
  {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

const scriptModel = model<IScripts>("scripts", scriptsSchema);

export default scriptModel;
