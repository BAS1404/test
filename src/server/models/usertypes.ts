import { Document, model, ObjectId, Schema } from "mongoose";

interface IUserTypes extends Document {
  _id: ObjectId;
  name: string;
}

const userTypesSchema = new Schema<IUserTypes>(
  {
    name: { type: String, required: true},
  },
  {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

const userTypesModel = model<IUserTypes>("usertypes", userTypesSchema);

export default userTypesModel;
