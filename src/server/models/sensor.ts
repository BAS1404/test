import { Document, model, ObjectId, Schema } from "mongoose";

interface ISensor extends Document {
  _id: ObjectId;
  name: string;
  description?: string;
  address?: string;
  zone?: string;
}

const sensorSchema = new Schema<ISensor>(
  {
    name: { type: String, required: true },
    description: { type: String, required: false },
  },
  {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

const sensorModel = model<ISensor>("devices", sensorSchema);

export default sensorModel;
