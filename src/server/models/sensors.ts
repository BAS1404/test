import { Document, model, ObjectId, Schema } from "mongoose";

interface ISensors extends Document {
  _id: ObjectId;
  name: ObjectId;
  timeEventSensorAt: Date;
  state?: ObjectId;
  user?: ObjectId;
  script?: ObjectId;
  skudEmployee?: ObjectId;
  createdAt: Date;
  updatedAt: Date;
}

const sensorsSchema = new Schema<ISensors>(
  {
    name: { type: Schema.Types.ObjectId, ref: "devices" },
    timeEventSensorAt: { type: Date, required: true },
    state: { type: Schema.Types.ObjectId, ref: "states" },
    user: { type: Schema.Types.ObjectId, ref: "users" },
    script: { type: Schema.Types.ObjectId, ref: "scripts" },
    skudEmployee: { type: Schema.Types.ObjectId, ref: "users" },
  },
  {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

const sensorsModel = model<ISensors>("sensors", sensorsSchema);

export default sensorsModel;
