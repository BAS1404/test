import { Document, model, ObjectId, Schema } from "mongoose";

interface IState extends Document {
  _id: ObjectId;
  name: string;
  description: string;
}

const stateSchema = new Schema<IState>(
  {
    name: { type: String, required: true },
    description: { type: String, required: false },
  },
  {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

const stateModel = model<IState>("states", stateSchema);

export default stateModel;
