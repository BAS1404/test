import "dotenv/config";
import { faker } from "@faker-js/faker";
import { connect, connection } from "mongoose";
import {
  ScriptModel,
  SensorModel,
  StateModel,
  UserTypesModel,
  UsersModel,
  SensorsModel,
} from "./models";

const {
  MONGO_ROOT_USERNAME,
  MONGO_ROOT_PASSWORD,
  MONGO_ROOT_ADDRESS,
  MONGO_ROOT_PORT,
  MONGO_ROOT_DATABASE,
} = process.env;

const simpleModel = (count: number, dataCreator: Object) => {
  const data = [...new Array(count).keys()];
  return data.map(() => dataCreator);
};

const getId = (item: { _id: any }) => item._id.toString();
const getIdAndType = (item: { _id: any; type?: any }) => ({
  id: item._id.toString(),
  type: item.type,
});

const random = (arr: Array<String>) => {
  return Math.floor(Math.random() * arr.length);
};

const createDB = async () => {
  const scriptIds = (
    await ScriptModel.insertMany(
      simpleModel(5, {
        name: faker.lorem.words(1),
        description: faker.lorem.words(3),
      })
    )
  ).map(getId);
  const sensorIds = (
    await SensorModel.insertMany(
      simpleModel(5, {
        name: faker.lorem.words(1),
        description: faker.lorem.words(3),
      })
    )
  ).map(getId);
  const stateIds = (
    await StateModel.insertMany(
      simpleModel(5, {
        name: faker.lorem.words(1),
        description: faker.lorem.words(3),
      })
    )
  ).map(getId);
  const userTypesIds = (
    await UserTypesModel.insertMany(
      simpleModel(2, {
        name: faker.lorem.words(1),
        description: faker.lorem.words(3),
      })
    )
  ).map(getId);
  const userIds = (
    await UsersModel.insertMany(
      simpleModel(15, {
        fio: `${faker.name.firstName} ${faker.name.middleName} ${faker.name.lastName}`,
        phone: faker.phone.phoneNumberFormat(),
        type: userTypesIds[random(userTypesIds)],
      })
    )
  ).map(getIdAndType);

  const usersIds = userIds.map((item) => item.id);
  const skudEmployeeIds = userIds
    .filter((item) => item.type.toString() === userTypesIds[1])
    .map((item) => item.id);

  await SensorsModel.insertMany(
    simpleModel(10000, {
      name: sensorIds[random(sensorIds)],
      timeEventSensorAt: faker.date.recent(365, "2020-01-01T00:00:00.000Z"),
      state: stateIds[random(stateIds)],
      user: usersIds[random(usersIds)],
      script: scriptIds[random(scriptIds)],
      skudEmployee: skudEmployeeIds[random(skudEmployeeIds)],
    })
  );
  process.exit(0);
};
connect(
  `mongodb://${MONGO_ROOT_USERNAME}:${MONGO_ROOT_PASSWORD}@${MONGO_ROOT_ADDRESS}:${MONGO_ROOT_PORT}`,
  {
    dbName: MONGO_ROOT_DATABASE,
  }
);
connection.on("connected", () => {
  console.log(`connected`);
  createDB();
});
