WORKDIR=$PWD
rm -rf $WORKDIR/dist;
cd $WORKDIR/src/server;
npm run build
cd $WORKDIR/src/client
npm run build
cd $WORKDIR/dist;
npm install
cp -f $PWD/src/server/.env $PWD/dist/.env
